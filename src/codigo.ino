#include <IRremote.h>
#include <LiquidCrystal.h>
#define IR 6
#define MOV 8
#define LUZ A0
#define BUZ 7

IRrecv receptor(IR);
decode_results resultado;
bool ligado = false;
bool senslumi = false;
bool senslumichange = false;
bool autoligar = false;
int luminosidade = 0, varlumi = 115;
long int tempo2 = 0;
int tempo1 = 0;
bool vexez = true;
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  lcd.begin(16, 2);
  receptor.enableIRIn();
  pinMode(MOV,INPUT);
  pinMode(BUZ,OUTPUT);
}

void loop() {
  
  
  ligadooudesligado();
  modoligado();
  modoluz();
  alarme();
  luminosida();
  mudancadeluminosidade();
  programarhorario();
  automaticoalarme();
}

void ligadooudesligado(){
	if(receptor.decode(&resultado)){
    if(resultado.value == 0xFD00FF){
      ligado = !ligado;
    	senslumi = false;
    }
    if(resultado.value == 16597183){
    	senslumi = !senslumi;
    }
      if(resultado.value == 16582903){
      	senslumichange = !senslumichange;
      }
      if(resultado.value == 16625743){
      	autoligar = !autoligar;
      }
    receptor.resume();
  }
  }


void modoligado(){
  if(ligado == true){lcd.setCursor(0,0);
  lcd.print("Alarme ON       ");  
            }
  if(ligado == false){
  lcd.setCursor(0,0);	
  lcd.print("Alarme OFF      ");
  }
}

void alarme(){
  if(ligado == true && digitalRead(MOV) == 1){
  	digitalWrite(BUZ,0);
    delay(100);
    digitalWrite(BUZ,1);
    delay(100);
  }
  if(ligado == false){
  	digitalWrite(BUZ,0);
  }
}

void luminosida(){
  if(senslumi == true){
    luminosidade = analogRead(LUZ);
    if(luminosidade > varlumi){
    	ligado = true;
    }else{
    	ligado = false;
    }
  }
}

void modoluz(){
  if(senslumi == true){lcd.setCursor(0,1);
  lcd.print("Modo Luz ON     ");                     
            }
  if(senslumi == false){
  	lcd.setCursor(0,1);
    lcd.print("Modo Luz OFF    ");
  }
}

void mudancadeluminosidade(){  
  while(senslumichange == true){
  	lcd.setCursor(0,0);
    lcd.print("Luminosidade   ");
    lcd.setCursor(0,1);
    lcd.print(varlumi);
    lcd.setCursor(3,1);
    lcd.print("          ");
    if(receptor.decode(&resultado)){
      
      	if(resultado.value == 0xFD00FF){
      	senslumichange = false;
          
    }
    	if(resultado.value == 16582903){
      	senslumichange = !senslumichange;
      }
      	if(resultado.value == 16601263){
          if(varlumi < 1000){
          varlumi = varlumi + 5;
          }
      }
      	if(resultado.value == 16584943){
          if(varlumi > 0){
          varlumi = varlumi - 5;
          }
      }
      	if(resultado.value == 16605343){
          if(varlumi < 901){
          varlumi = varlumi + 100;
          }
      }
      	if(resultado.value == 16589023){
          if(varlumi > 99){
          varlumi = varlumi - 100;
          }
      }
      receptor.resume();
    }
  }
}

void programarhorario(){
  while(autoligar == true){
  	
    lcd.setCursor(0,0);
    lcd.print("ON/OFF depois de");
    if(receptor.decode(&resultado)){
    	tempo1 = amogus(tempo1);
      if(resultado.value == 16625743){
      	tempo2 = millis() + (tempo1*100);
        vexez = false;
        autoligar = !autoligar;
      }
  }
    if(millis() > tempo2 && tempo2 != 0){
    	ligado = !ligado;
    }
    lcd.setCursor(0,1);
    lcd.print(tempo1);
  	lcd.print(" segundos     ");
    
}	
}

int amogus(int ex){

if(receptor.decode(&resultado)){
  if(resultado.value == 0xFD00FF){
    autoligar = false;  
    
    }  	
  if(resultado.value == 16601263){
          if(ex < 1000){
          ex = ex + 5;
          }
      }
      	if(resultado.value == 16584943){
          if(ex > 0){
          ex = ex - 5;
          }
      }
      	if(resultado.value == 16605343){
          if(ex < 959){
          ex = ex + 60;
          }
      }
      	if(resultado.value == 16589023){
          if(ex > 59){
          ex = ex - 60;
          }
      }
      receptor.resume();
    }
	return ex;
}

void automaticoalarme(){
	if(millis() > tempo2 && tempo2 != 0 && vexez == false){
    
      ligado = !ligado;
      vexez = !vexez;
    }
}
