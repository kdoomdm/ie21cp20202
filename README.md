# Introdução   

O circuito montado é de um Sistema de Segurança com sensor de movimento, sensor de luminosidade e temporizador.

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação!

|Nome| gitlab user|
|---|---|
|Kevin Dias Okuma|@kdoomdm|
|Thiago Gonçalves Salustiano|@tigocomtigo|

# Foto/Video 

Clique na foto para ver o vídeo completo.

[![](img/imagem_ligado.png)](https://www.youtube.com/watch?v=ANZj_CS_YQI)

# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://kdoomdm.gitlab.io/ie21cp20202

## Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)
